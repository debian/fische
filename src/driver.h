/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 * 
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRIVER_H
#define DRIVER_H

#ifdef HAVE_CONFIG_H
	#include "config.h"
	#if defined(HAVE_ALSA_ASOUNDLIB_H) && defined(HAVE_LIBASOUND)
		#define HAVE_ALSA
	#endif
	#if defined(HAVE_PULSE_SIMPLE_H) && defined(HAVE_LIBPULSE_SIMPLE)
		#define HAVE_PULSE
	#endif
	#if defined(HAVE_PORTAUDIO_H) && defined(HAVE_LIBPORTAUDIO)
		#define HAVE_PORTAUDIO
	#endif

// without config.h assume all drivers present
#else // HAVE_CONFIG_H
	#define HAVE_ALSA
	#define HAVE_PULSE
	#undef HAVE_PORTAUDIO
#endif // HAVE_CONFIG_H

// on windows or OSX - no autodetection.
#if defined(__WIN32__) || defined(__APPLE__)
	#undef HAVE_ALSA
	#undef HAVE_PULSE
	#define HAVE_PORTAUDIO
#endif // __WIN32__

#endif // DRIVER_H
	