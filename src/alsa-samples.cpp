
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "driver.h"

#ifdef HAVE_ALSA
#include <alsa/asoundlib.h>
#endif // HAVE_ALSA

#include <iostream>
#include <cstdlib>
#include "alsa-samples.h"
#include "ringbuffer.h"

/* workaround to enable build on kFreeBsd */
#ifndef EBADFD
#define EBADFD 77
#endif

using namespace std;

#ifdef HAVE_ALSA

int sample_alsa_pcm (void* arg)
{
	sample_param_t* p = (sample_param_t*) arg;
	RingBuffer* ringbuffer = p->ringbuffer;
	const char* device = p->device;
	bool* run = p->run;
	int err;
	unsigned int rate = 44100;
	char *buf = (char*) malloc (128);
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_t *capture_handle;

	if ( (err = snd_pcm_open (&capture_handle, device, SND_PCM_STREAM_CAPTURE, SND_PCM_NONBLOCK)) < 0) {
		cerr << "ERROR: cannot open audio device \"" << device << "\": " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
		cerr << "ERROR: cannot allocate hardware parameter structure: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_any (capture_handle, hw_params)) < 0) {
		cerr << "ERROR: cannot initialize hardware parameter structure: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_set_access (capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
		cerr << "ERROR: cannot set access type: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_set_format (capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
		cerr << "ERROR: cannot set sample format: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &rate, 0)) < 0) {
		cerr << "ERROR: cannot set sample rate: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params_set_channels (capture_handle, hw_params, 2)) < 0) {
		cerr << "ERROR: cannot set channel count: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	if ( (err = snd_pcm_hw_params (capture_handle, hw_params)) < 0) {
		cerr << "ERROR: cannot set parameters: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}
	snd_pcm_hw_params_free (hw_params);
	if ( (err = snd_pcm_prepare (capture_handle)) < 0) {
		cerr << "ERROR: cannot prepare audio interface for use: " << snd_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}

	cout << "* created thread for sound sample acquisition (alsa)" << endl;

	int n = 0;
	int c = 0;

	while (*run) {
		n = snd_pcm_readi (capture_handle, buf, 32);
		// device not in correct state
		if ( (n == -EBADFD) || (n == -EPIPE)) {
			err = snd_pcm_prepare (capture_handle);
			if (err < 0) {
				cerr << "ERROR: cannot re-prepare audio interface for use: " << snd_strerror (err) << endl;
				exit (EXIT_FAILURE);
			} else if (c > 10) {
				cerr << "Unrecoverable ALSA error (" << snd_strerror (n) << "). giving up." << endl;
				exit (EXIT_FAILURE);
			}
			// increment error counter
			c ++;
		}
		// reading too early - no data available yet
		else if ( (n == -EAGAIN) || (n == 0)) usleep (1000);
		// other error
		else if (n < 0) {
			cerr << "ALSA ERROR " << -n << " (" << snd_strerror (n) << ")" << endl;
			cerr << "Please report this bug to fische at 26elf dot at." << endl;
			exit (EXIT_FAILURE);
		}
		// everything's fine
		else if (n > 0) {
			// write data
			ringbuffer->insert (buf, n * 4);
			// reset error counter
			c = 0;
		}
	}
	cout << "* stopping sound sampling thread" << endl;
	snd_pcm_close (capture_handle);
	free (buf);
	buf = NULL;
	return NULL;
}

#else // HAVE_ALSA
int sample_alsa_pcm (void* arg)
{
	cout << "ERROR: ALSA driver not enabled at compile time." << endl;
	cout << "       Please use a different audio input driver!" << endl;
	exit (EXIT_FAILURE);
}
#endif // HAVE_ALSA
