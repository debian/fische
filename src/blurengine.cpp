/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "blurengine.h"
#include "vectorfield.h"
#include "sdlscreen.h"
#include "configuration.h"

extern "C" {
#include "traditional.h"
}

#include <SDL/SDL.h>

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif // __APPLE__

using namespace std;

BlurEngine* BlurEngine::ms_instance = 0;

BlurEngine::BlurEngine()
{
	mp_SdlScreen = SdlScreen::instance();
	mp_Vectorfield = VectorField::instance();
	m_X0 = (mp_SdlScreen->width() - mp_Vectorfield->width()) / 2;
	m_Y0 = (mp_SdlScreen->height() - mp_Vectorfield->height()) / 2;
	m_Width = mp_Vectorfield->width();
	m_Height = mp_Vectorfield->height();
	mp_Buffer = malloc (m_Width * m_Height * 4);
	if (!mp_Buffer) {
		cerr << "ERROR: unable to allocate memory for graphics buffer." << endl;
		exit (EXIT_FAILURE);
	}
	
	m_Cpus = Configuration::instance()->processorCores();
}


BlurEngine::~BlurEngine()
{
	free (mp_Buffer);
	mp_Buffer = NULL;
}

BlurEngine* BlurEngine::instance()
{
	if (!ms_instance) ms_instance = new BlurEngine();
	return ms_instance;
}

void BlurEngine::release()
{
	delete ms_instance;
	ms_instance = 0;
}

void BlurEngine::blur()
{
	mp_SdlScreen->lock();

	if (m_Cpus == 1) blurTrad();
	else blurTrad_MT();

	writeBuffer();
	mp_SdlScreen->unlock();
}

void BlurEngine::blurTrad()
{
	static blur_param_t p;
	p.pixels = mp_SdlScreen->pixels();
	p.width = m_Width;
	p.y0 = m_Y0;
	p.x0 = m_X0;
	p.yMin = 0;
	p.yMax = m_Height;
	p.pitch = mp_SdlScreen->width();
	p.vectors = (int8_t*) mp_Vectorfield->get();
	p.buffer = mp_Buffer;
	traditional_blur (&p);
}

void BlurEngine::blurTrad_MT()
{
	int n = m_Cpus;
	if (n > 8) n = 8;
	if (n % 2 != 0) n--;

	SDL_Thread* t[n];

	blur_param_t p[n];
	for (int i = 0; i < n; i ++) {
		p[i].pixels = mp_SdlScreen->pixels();
		p[i].width = m_Width;
		p[i].y0 = m_Y0;
		p[i].x0 = m_X0;
		p[i].pitch = mp_SdlScreen->width();
		p[i].vectors = (int8_t*) mp_Vectorfield->get();
		p[i].buffer = mp_Buffer;
		p[i].yMin = (i * m_Height) / n;
		p[i].yMax = ( (i + 1) * m_Height) / n;

		t[i] = SDL_CreateThread (traditional_blur, &p[i]);
	}
	for (int i = 0; i < n; i ++) {
		SDL_WaitThread (t[i], NULL);
	}
}

void BlurEngine::emptyBuffer()
{
	memset (mp_Buffer, 0, m_Width * m_Height * 4);
}

void BlurEngine::writeBuffer()
{
	uint32_t* pixels = (uint32_t*) mp_SdlScreen->pixels();
	uint32_t* src = (uint32_t*) mp_Buffer;
	int pitch = mp_SdlScreen->width();
	for (int y = 0; y < m_Height; y ++) {
		memcpy (pixels + (y + m_Y0) * pitch + m_X0, src + y * m_Width, m_Width * 4);
	}
}
