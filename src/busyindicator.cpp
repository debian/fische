
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PAR
 * TICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL/SDL.h>
#include <iostream>
#include <unistd.h>
#include "sdlscreen.h"
#include "vector.h"

void line (SdlScreen* screen, int x1, int y1, int x2, int y2, uint32_t color)
{
	int Dx = abs (x1 - x2);
	int Dy = abs (y1 - y2);
	int dx = (x2 < x1) ? -1 : 1;
	int dy = (y2 < y1) ? -1 : 1;
	uint32_t* pixels = (uint32_t*) screen->pixels();
	int pitch = screen->width();

	if ( (Dx == 0) && (Dy == 0)) return;

	if (Dx > Dy) {
		for (int x = x1; x * dx <= x2 * dx; x += dx) {
			int y = (int) ( (double) y1 + (double) Dy / (double) Dx * (double) dy * (double) abs (x - x1) + 0.5);
			* (pixels + y * pitch + x) = color;
		}
	} else {
		for (int y = y1; y * dy <= y2 * dy; y += dy) {
			int x = (int) ( (double) x1 + (double) Dx / (double) Dy * (double) dx * (double) abs (y - y1) + 0.5);
			* (pixels + y * pitch + x) = color;
		}
	}
}

int busy_indicator (void* p)
{
	static double* progress = (double*) p;
	static SdlScreen* screen = SdlScreen::instance();
	static double x0 = screen->width() / 2;
	static double y0 = screen->height() / 2;
	static double dim = (x0 > y0) ? y0 / 2 : x0 / 2;
	static Vector center (x0, y0);
	
	double last = -1;
	
	while (*progress < 1) {
		if (*progress == last) {
			usleep (10000);
			continue;
		}
		last = *progress;
		double angle = * progress * -2 * 3.1415 + 3.0415;
		Vector c1 (sin (angle) * dim, cos (angle) * dim);
		Vector c2 (sin (angle + 0.1) * dim, cos (angle + 0.1) * dim);
		Vector e1 = c1.singleUnit();
		Vector e2 = c2.singleUnit();
		Vector c3 = c2 - e2 * dim / 2;
		Vector c4 = c1 - e1 * dim / 2;
		c1 += center;
		c2 += center;
		c3 += center;
		c4 += center;
		screen->lock();
		line (screen, c1.x(), c1.y(), c2.x(), c2.y(), 0xffffffff);
		line (screen, c2.x(), c2.y(), c3.x(), c3.y(), 0xffffffff);
		line (screen, c3.x(), c3.y(), c4.x(), c4.y(), 0xffffffff);
		line (screen, c4.x(), c4.y(), c1.x(), c1.y(), 0xffffffff);
		screen->unlock();
		screen->update();
		usleep (10000);
	}
	return 0;
}
