
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef WIN32
// please note that fische will not compile or run on windows
// this is for development reasons only
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif // WIN32
#include <iostream>
#include <stdexcept>
#include "sdlscreen.h"

using namespace std;

SdlScreen* SdlScreen::ms_instance = 0;

SdlScreen::SdlScreen (int x, int y, bool fs) {
	m_Width = x;
	m_Height = y;
	m_IsFullscreen = fs;

	if (SDL_Init (SDL_INIT_VIDEO) != 0) {
		cerr << "ERROR: could not initialize SDL" << endl;
		exit (EXIT_FAILURE);
	}

	int flags = SDL_SWSURFACE;
	if (m_IsFullscreen) flags |= SDL_FULLSCREEN;

	mp_Screen = SDL_SetVideoMode (m_Width, m_Height, 32, flags);

	if (!mp_Screen) {
		cerr << "ERROR: could not open requested video mode" << endl;
		exit (EXIT_FAILURE);
	}

	// do not show the mouse cursor
	SDL_ShowCursor (SDL_DISABLE);

	// leave cursor and keyboard available to other apps
	SDL_WM_GrabInput (SDL_GRAB_OFF);

	int bpp = mp_Screen->format->BitsPerPixel;
	if (bpp != 32) {
		cerr << "ERROR: could not set 32 bit pixel format" << endl;
		exit (EXIT_FAILURE);
	}

	m_Width = mp_Screen->w;
	m_Height = mp_Screen->h;

	cout << "* initialized " << mp_Screen->w << "x" << mp_Screen->h << "x" << bpp << " SDL surface with flags: ";
	if (mp_Screen->flags & SDL_HWSURFACE) cout << "HWSURFACE";
	else cout << "SWSURFACE";
	if (mp_Screen->flags & SDL_ASYNCBLIT) cout << "|ASYNCBLIT";
	if (mp_Screen->flags & SDL_ANYFORMAT) cout << "|ANYFORMAT";
	if (mp_Screen->flags & SDL_HWPALETTE) cout << "|HWPALETTE";
	if (mp_Screen->flags & SDL_DOUBLEBUF) cout << "|DOUBLEBUF";
	if (mp_Screen->flags & SDL_FULLSCREEN) cout << "|FULLSCREEN";
	if (mp_Screen->flags & SDL_OPENGL) cout << "|OPENGL";
	if (mp_Screen->flags & SDL_OPENGLBLIT) cout << "|OPENGLBLIT";
	if (mp_Screen->flags & SDL_RESIZABLE) cout << "|RESIZABLE";
	if (mp_Screen->flags & SDL_HWACCEL) cout << "|HWACCEL";
	if (mp_Screen->flags & SDL_SRCCOLORKEY) cout << "|SRCCOLORKEY";
	if (mp_Screen->flags & SDL_RLEACCEL) cout << "|RLEACCEL";
	if (mp_Screen->flags & SDL_SRCALPHA) cout << "|SRCALPHA";
	if (mp_Screen->flags & SDL_PREALLOC) cout << "|PREALLOC";
	cout << endl;

	SDL_WM_SetCaption ("fische 3.2", "fische 3.2");
}

SdlScreen::~SdlScreen() {
	SDL_ShowCursor (SDL_ENABLE);
	SDL_FreeSurface (mp_Screen);
	SDL_Quit();
}

void SdlScreen::release() {
	delete ms_instance;
	ms_instance = 0;
}

SdlScreen* SdlScreen::instance (int x, int y, bool fs) {
	if (!ms_instance) ms_instance = new SdlScreen (x, y, fs);
	return ms_instance;
}

void SdlScreen::update() {
	SDL_UpdateRect (mp_Screen, 0, 0, 0, 0);
}

int SdlScreen::height() const {
	return m_Height;
}

int SdlScreen::width() const {
	return m_Width;
}

void* SdlScreen::pixels() {
	return mp_Screen->pixels;
}

void SdlScreen::lock() {
	SDL_LockSurface (mp_Screen);
}

void SdlScreen::unlock() {
	SDL_UnlockSurface (mp_Screen);
}

int SdlScreen::pitch() const {
	return mp_Screen->pitch;
}

bool SdlScreen::toggleFullscreen() {
	bool success = true;
	uint32_t flags = mp_Screen->flags;
	mp_Screen = SDL_SetVideoMode (0, 0, 0, mp_Screen->flags ^ SDL_FULLSCREEN);
	if (!mp_Screen) {
		mp_Screen = SDL_SetVideoMode (0, 0, 0, flags);
		success = false;
	}
	if (!mp_Screen) throw runtime_error ("Fullscreen switching error");
	return success;
}

int SdlScreen::checkEvents() {
	SDL_Event event;
	int retval = CONTINUE;

	while (SDL_PollEvent (&event) ) {
		switch (event.type) {
			case SDL_MOUSEBUTTONDOWN:
				retval |= MOUSE;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						retval |= EXIT;
						break;
					case SDLK_p:
						retval |= PAUSE;
						break;
					case SDLK_f:
						retval |= TOGGLE_FULLSCREEN;
						break;
					case SDLK_n:
						retval |= TOGGLE_NERVOUS;
						break;
					case SDLK_UP:
						retval |= PLUS;
						break;
					case SDLK_DOWN:
						retval |= MINUS;
						break;
					default:
						break;
				}
				break;
			case SDL_QUIT:
				retval |= EXIT;
				break;
			default:
				break;
		}
	}
	return ( retval );
}
