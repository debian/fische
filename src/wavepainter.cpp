/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdint.h>
#include <cstdlib>
#include <cmath>
#include "sdlscreen.h"
#include "vectorfield.h"
#include "ringbuffer.h"
#include "wavepainter.h"
#include "configuration.h"

using namespace std;

WavePainter* WavePainter::ms_instance = 0;

WavePainter::WavePainter ()
{
	VectorField* v = VectorField::instance();
	SdlScreen* s = SdlScreen::instance();

	m_Height = v->height();
	m_Width = v->width();
	m_Y0 = (s->height() - v->height()) / 2;
	m_X0 = (s->width() - v->width()) / 2;
	mp_RingBuffer = RingBuffer::instance();
	mp_SdlScreen = s;
	m_CenterX = m_X0 + m_Width / 2;
	m_CenterY = m_Y0 + m_Height / 2;
	m_Shape = 1;
	m_Shapes = 2;
	m_Color1 = rand() % 0xffffffff;
	m_Color2 = ~m_Color1;
	m_Angle = 0;
	m_Direction = 1;
	m_IsRotating = false;
	m_RotationIncrement = 0;
}

WavePainter* WavePainter::instance ()
{
	if (!ms_instance) ms_instance = new WavePainter();
	return ms_instance;
}

void WavePainter::line (int x1, int y1, int x2, int y2, uint32_t color)
{
	static uint32_t* pixels = (uint32_t*) mp_SdlScreen->pixels();
	static int pitch = mp_SdlScreen->width();

	int Dx = (x1 > x2) ? (x1 - x2) : (x2 - x1);
	int Dy = (y1 > y2) ? (y1 - y2) : (y2 - y1);
	int dx = (x2 < x1) ? -1 : 1;
	int dy = (y2 < y1) ? -1 : 1;

	if (!Dx && !Dy) return;

	if (Dx > Dy) {
		for (int x = x1; x * dx <= x2 * dx; x += dx) {
			int y = (int) ( (double) y1 + (double) Dy / (double) Dx * (double) dy * (double) abs (x - x1) + 0.5);
			if ( (x < m_X0) || (x >= m_X0 + m_Width) || (y < m_Y0) || (y >= m_Y0 + m_Height)) continue;
			* (pixels + y * pitch + x) = color;
		}
	} else {
		for (int y = y1; y * dy <= y2 * dy; y += dy) {
			int x = (int) ( (double) x1 + (double) Dx / (double) Dy * (double) dx * (double) abs (y - y1) + 0.5);
			if ( (x < m_X0) || (x >= m_X0 + m_Width) || (y < m_Y0) || (y >= m_Y0 + m_Height)) continue;
			* (pixels + y * pitch + x) = color;
		}
	}
}

void WavePainter::paint ()
{
	const double Pi = acos (-1);
	static double f = 0;
	if (m_IsRotating) {
		m_Angle += m_RotationIncrement;
		if ( (m_Angle > 2 * Pi) || (m_Angle < -2 * Pi)) {
			m_Angle = 0;
			m_IsRotating = false;
		}
	}

	int16_t* samples = (int16_t*) mp_RingBuffer->get();
	int l = mp_RingBuffer->size() / 4;

	int dim = (m_Height < m_Width) ? m_Height : m_Width;
	int div = 196605 / dim;

	switch (m_Shape) {
		case 0: {
			Point base1 (m_Width / 2 + dim / 6 * sin (m_Angle), m_Height / 2 + dim / 6 * cos (m_Angle));
			Vector n = base1 - Vector (m_Width / 2, m_Height / 2);
			n = n.normal();
			Point start1 = base1.intersectBorder (n, m_Width, m_Height, Vector::LEFT);
			Point end1 = base1.intersectBorder (n, m_Width, m_Height, Vector::RIGHT);

			Point base2 (m_Width / 2 - dim / 6 * sin (m_Angle), m_Height / 2 - dim / 6 * cos (m_Angle));
			n = base2 - Vector (m_Width / 2, m_Height / 2);
			n = n.normal();
			Point start2 = base2.intersectBorder (n, m_Width, m_Height, Vector::LEFT);
			Point end2 = base2.intersectBorder (n, m_Width, m_Height, Vector::RIGHT);

			Vector v1 = end1 - start1;
			v1 /= l;
			Vector v2 = end2 - start2;
			v2 /= l;
			Vector n1 = v1.singleUnit().normal();
			Vector n2 = v2.singleUnit().normal();

			Point base, pt1, pt2;
			for (int i = 0; i < l - 1; i ++) {
				base = start1 + v1 * i;
				pt1 = base + n1 * (* (samples + 2 * i) / div);
				base += v1;
				pt2 = base + n1 * (* (samples + 2 * (i + 1)) / div);
				line (pt1, pt2, m_Color1);
				base = start2 + v2 * i;
				pt1 = base + n2 * (* (samples + 1 + 2 * i) / div);
				base += v2;
				pt2 = base + n2 * (* (samples + 1 + 2 * (i + 1)) / div);
				line (pt1, pt2, m_Color2);
			}
			return;
		}
		case 1:
			f = cos (Pi / 3 + 2 * m_Angle) + 0.5;
			for (int i = 0; i < l - 1; i ++) {
				double phi1 = Pi * (0.25 + (double) i / (double) l) + m_Angle;
				double phi2 = phi1 + Pi / (double) l;
				double r1 = dim / 4 + * (samples + 2 * i) / div;
				double r2 = dim / 4 + * (samples + 2 * (i + 1)) / div;
				int x1 = roundToInt (m_CenterX + f * r1 * sin (phi1));
				int x2 = roundToInt (m_CenterX + f * r2 * sin (phi2));
				int y1 = roundToInt (m_CenterY + r1 * cos (phi1));
				int y2 = roundToInt (m_CenterY + r2 * cos (phi2));
				line (x1, y1, x2, y2, m_Color1);
				phi1 += Pi;
				phi2 += Pi;
				r1 = dim / 4 + * (samples + 1 + 2 * i) / div;
				r2 = dim / 4 + * (samples + 1 + 2 * (i + 1)) / div;
				x1 = roundToInt (m_CenterX + f * r1 * sin (phi1));
				x2 = roundToInt (m_CenterX + f * r2 * sin (phi2));
				y1 = roundToInt (m_CenterY + r1 * cos (phi1));
				y2 = roundToInt (m_CenterY + r2 * cos (phi2));
				line (x1, y1, x2, y2, m_Color2);
			}
			return;
	}
}

void WavePainter::changeShape()
{
	if (m_IsRotating) return;
	int n = rand() % m_Shapes;
	while (n == m_Shape) n = rand() % m_Shapes;
	m_Shape = n;
}

void WavePainter::beat (double bpm)
{
	const double Pi = acos (-1);
	if (!m_IsRotating) {
		if (bpm) {
			m_Direction = 1 - 2 * (rand() % 2);
			m_RotationIncrement = Pi / bpm / 2 * m_Direction;
			m_Angle = 0;
			m_IsRotating = true;
		}
	}
}

void WavePainter::changeColor (double bpm, double energy)
{
	if (!bpm && !energy) {
		m_Color1 = rand() % 0xffffffff;
		m_Color2 = ~m_Color1;
	}
	if (!bpm)
		return;
	double hue = bpm / 5;
	while (hue >= 6)
		hue -= 6;
	double sv = (energy > 1) ? 1 : pow (energy, 4);
	double x = sv * (1 - abs ( ( (int) hue) % 2 - 1));

	double r, g, b;

	switch ( (int) hue) {
		case 0:
			r = sv;
			g = x;
			b = 0;
			break;
		case 1:
			r = x;
			g = sv;
			b = 0;
			break;
		case 2:
			r = 0;
			g = sv;
			b = x;
			break;
		case 3:
			r = 0;
			g = x;
			b = sv;
			break;
		case 4:
			r = x;
			g = 0;
			b = sv;
			break;
		default:
		case 5:
			r = sv;
			g = 0;
			b = x;
	}

	uint32_t red = floor (r * 255 + 0.5);
	uint32_t green = floor (b * 255 + 0.5);
	uint32_t blue = floor (g * 255 + 0.5);
	
	m_Color1 = (0xff << 24) + (red << 16) + (green << 8) + blue;
	m_Color2 = ~m_Color1;
}

int roundToInt (double v)
{
	if (v > 0) return (int) (v + 0.5);
	else return (int) (v - 0.5);
}
