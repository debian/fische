
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <cmath>
//#include <iostream>
#include <algorithm>
#include "analyst.h"
#include "ringbuffer.h"
#include "configuration.h"

using namespace std;

Analyst* Analyst::ms_instance = 0;

Analyst::Analyst()
{
	mp_RingBuffer 		= RingBuffer::instance();
	mp_Configuration 	= Configuration::instance();
	m_MovingAverage30 	= 0;
	m_MovingAverage03 	= 0;
	m_StandardDeviation = 0;
	m_IntensityMa 		= 0;
	m_IntensityStd 		= 0;
	m_State 			= WAITING;
	m_FrameNumber 		= 0;
	m_FramesPerBeat 	= 0;
}

Analyst::~Analyst()
{
	m_BeatGapHistory.clear();
}

Analyst* Analyst::instance()
{
	if (!ms_instance) ms_instance = new Analyst();
	return ms_instance;
}

void Analyst::release()
{
	delete ms_instance;
	ms_instance = 0;
}

int Analyst::analyse()
{
	m_FrameNumber ++;

	double relative_intensity 	= 0;
	int16_t* samples 			= (int16_t*) mp_RingBuffer->get();
	double dezibel 				= level (samples);

	if (m_MovingAverage30 == 0)
		m_MovingAverage30 		= dezibel;
	else
		m_MovingAverage30 		= m_MovingAverage30 * 0.9667 + dezibel * 0.0333;

	m_StandardDeviation 		= m_StandardDeviation * 0.9667 + abs (dezibel - m_MovingAverage30) * 0.0333;

	if ( (m_FrameNumber - m_LastBeatFrame) > 90) {
		m_FramesPerBeat = 0;
		m_BeatGapHistory.clear();
	}

	double new_frames_per_beat;

	switch (m_State) {
		case WAITING:
			// don't bother if intensity too low
			if (dezibel < m_MovingAverage30 + m_StandardDeviation) break;

			// initialisation fallbacks
			if (m_StandardDeviation == 0)
				relative_intensity = 1; // avoid div by 0
			else
				relative_intensity = (dezibel - m_MovingAverage30) / m_StandardDeviation;

			if (m_IntensityMa == 0)
				m_IntensityMa = relative_intensity; // initial assignment
			else
				m_IntensityMa = m_IntensityMa * 0.95 + relative_intensity * 0.05;

			// update intensity standard deviation
			m_IntensityStd = m_IntensityStd * 0.95 + abs (m_IntensityMa - relative_intensity) * 0.05;

			// we DO have a beat
			m_State = BEAT;

			// update beat gap history
			m_BeatGapHistory.push_back (m_FrameNumber - m_LastBeatFrame);
			if (m_BeatGapHistory.size() > 30) m_BeatGapHistory.erase (m_BeatGapHistory.begin());

			// remember this as the last beat
			m_LastBeatFrame = m_FrameNumber;

			// reset the short-term moving average
			m_MovingAverage03 = dezibel;

			// try a guess at the tempo
			new_frames_per_beat = guessFramesPerBeat();
			if ( (m_FramesPerBeat) && (m_FramesPerBeat / new_frames_per_beat < 1.2) && (new_frames_per_beat / m_FramesPerBeat < 1.2))
				m_FramesPerBeat = (m_FramesPerBeat * 2 + new_frames_per_beat) / 3;
			else
				m_FramesPerBeat = new_frames_per_beat;

			// return based on relative beat intensity
			if (relative_intensity > m_IntensityMa + 3 * m_IntensityStd)
				return 4;
			if (relative_intensity > m_IntensityMa + 2 * m_IntensityStd)
				return 3;
			if (relative_intensity > m_IntensityMa + 1 * m_IntensityStd)
				return 2;
			
			return 1;

		case BEAT:
		case MAYBEWAITING:
			// update short term moving average
			m_MovingAverage03 = m_MovingAverage03 * 0.6667 + dezibel * 0.3333;
			
			// needs to be low enough twice to exit BEAT state
			if (m_MovingAverage03 < m_MovingAverage30 + m_StandardDeviation) {
				if (m_State == MAYBEWAITING)
					m_State = WAITING;
				else
					m_State = MAYBEWAITING;
				return 0;
			}
	}
	
	// report level too low
	if (dezibel < -45) return -1;
	return 0;
}

double Analyst::level (int16_t* samples)
{
	double E = 0;
	for (int ii = 0; ii < mp_RingBuffer->size() / 2; ii += 2) {
		E += abs ( (double) * (samples + ii));
		E += abs ( (double) * (samples + ii + 1));
	}
	if (E <= 0) E = 1;
	E = E / mp_RingBuffer->size() * 2;
	return log10 (E / 32768) * 10;
}

double Analyst::guessFramesPerBeat()
{
	if (m_BeatGapHistory.size() < 30) return 0;
	vector<int> gap;
	gap = m_BeatGapHistory;
	sort (gap.begin(), gap.end());
	int guess = gap.at (14);
	double result = 0;
	int n = 0;
	for (unsigned int i = 0; i < gap.size(); i ++) {
		if (abs (gap.at (i) - guess) <= 2) {
			result += gap.at (i);
			n ++;
		}
	}
	return result / n;
}
