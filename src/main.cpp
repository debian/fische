/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __WIN32__
#include <SDL_main.h>
#endif // __WIN32__

#include <SDL/SDL.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <stdint.h>
#include <ctime>
#include <signal.h>
#include "sdlscreen.h"
#include "vectorfield.h"
#include "ringbuffer.h"
#include "blurengine.h"
#include "alsa-samples.h"
#include "pulseaudio-samples.h"
#include "portaudio-samples.h"
#include "dummyaudio-samples.h"
#include "wavepainter.h"
#include "analyst.h"
#include "configuration.h"
#include "busyindicator.h"

using namespace std;

bool g_sigint = false;

// enable clean exit on Ctrl^C
void sigint_caught (int)
{
	cout << "\n* caught SIGINT" << endl;
	g_sigint = true;
}

int main (int argc, char** argv)
{
	signal (SIGINT, sigint_caught);

	bool paused = false;
	uint32_t frames = 0;
	uint32_t ticks = 0;

	// initialize random number generator
	srand (time (NULL));

	// read options from commandline (or file - TODO)
	Configuration* configuration = Configuration::instance (argc, argv);

	// create ringbuffer
	RingBuffer* ringbuffer = RingBuffer::instance (173460 / configuration->fpsTarget());

	// create sampling thread
	static bool run = true;

	SDL_Thread* samplingThread;

	sample_param_t p;
	p.device = configuration->audioDevice();
	p.ringbuffer = ringbuffer;
	p.run = &run;

	switch (configuration->audioDriver()) {
		case Configuration::ALSA:
			samplingThread = SDL_CreateThread (sample_alsa_pcm, &p);
			break;
		case Configuration::PULSE:
			samplingThread = SDL_CreateThread (sample_pulseaudio, &p);
			break;
		case Configuration::PORTAUDIO:
			samplingThread = SDL_CreateThread (sample_portaudio, &p);
			break;
		case Configuration::DUMMY:
		default:
			samplingThread = SDL_CreateThread (sample_dummyaudio, &p);
			break;
	}

	// initialize display
	SdlScreen* sdlscreen = SdlScreen::instance (configuration->virtualWidth(), configuration->virtualHeight(), configuration->fullscreen());

	// indicate busy
	double progress = 0;
	SDL_Thread* busyThread = SDL_CreateThread (busy_indicator, &progress);

	// create vectors
	VectorField* vectorfield = VectorField::instance (configuration->width(), configuration->height(), &progress);
	if ( (vectorfield->width() > sdlscreen->width()) || (vectorfield->height() > sdlscreen->height())) {
		cerr << "ERROR: animation is bigger than screen can allow." << endl;
		exit (EXIT_FAILURE);
	}

	// end busy state
	progress = 1;
	SDL_WaitThread (busyThread, NULL);

	BlurEngine* blurengine = BlurEngine::instance();
	WavePainter* wavepainter = WavePainter::instance();
	Analyst* analyst = Analyst::instance();

	int events;
	int analysis = 0;
	// main loop
	while (!g_sigint && ! ( (events = sdlscreen->checkEvents()) & EXIT)) {
		// check for mouse exit event if enabled
		if ( (events & MOUSE) && configuration->exitOnMouse()) break;

		// check for interesting events
		if (events & TOGGLE_FULLSCREEN) {
			if (sdlscreen->toggleFullscreen())
				configuration->toggleFullscreen();
		}
		if (events & TOGGLE_NERVOUS) configuration->toggleNervous();
		if (events & PAUSE) paused = paused ? false : true;

		// paused? do nothing.
		if (paused) {
			usleep (10000);
			continue;
		}

		// wait until ringbuffer is full
		int sleepcount = 0;
		while (!ringbuffer->ready()) {
			usleep (1000);
			sleepcount ++;
			if (sleepcount > 100) break;
		}

		// ringbuffer not filling? in the meantime, check for new events.
		if (sleepcount > 100) continue;

		// analyse sound data
		if (configuration->audioDriver() == Configuration::DUMMY) {
			if (frames % 15 == 0) wavepainter->changeShape();
			if (frames % 150 == 0) vectorfield->change();
			if (frames % 450 == 0) wavepainter->beat (15);
		} else {
			analysis = analyst->analyse();
			if (configuration->nervous()) {
				if (analysis >= 2) wavepainter->changeShape();
				if (analysis >= 1) vectorfield->change();
			} else {
				if (analysis >= 1) wavepainter->changeShape();
				if (analysis >= 2) vectorfield->change();
			}
			if (analysis >= 3) wavepainter->beat (analyst->framesPerBeat());
		}

		// have fun
		wavepainter->changeColor (analyst->framesPerBeat(), analyst->relativeEnergy());
		if (analysis != -1)
			wavepainter->paint();
		sdlscreen->update();
		blurengine->blur();
		frames ++;
		if (frames == 1) ticks = SDL_GetTicks();
	}

	// print out actual frames per second
	cout << "\n* actual fps was " << (double) (frames - 1) / (double) (SDL_GetTicks() - ticks) * 1000 << endl;

	// tell the sound sampling thread to quit
	run = false;
	SDL_WaitThread (samplingThread, NULL);

	analyst->release();
	blurengine->release();
	sdlscreen->release();
	vectorfield->release();
	ringbuffer->release();
	configuration->release();

	exit (EXIT_SUCCESS);
}
