
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VECTORFIELD_H
#define VECTORFIELD_H

#include <vector>
#include <stdint.h>
#include "vector.h"

class VectorField
{
	private:
		static VectorField* ms_instance;

	private:
		VectorField (int, int, double*);
		~VectorField();

	public:
		static VectorField* instance (int x = 300, int y = 200, double* progress = NULL);
		void release();

	public:
		void change();
		int width() const;
		int height() const;
		char* get();
		int dimension() const;
		int diagonal() const;

	private:
		std::vector<uint16_t*>	m_Fields;
		uint16_t*				pExportData;
		int						m_Width;
		int						m_Height;
		int						m_Diagonal;
		int						m_CenterX;
		int						m_CenterY;

		inline void validate (Vector& v, int x, int y) {
			Vector sx (1, 0);
			Vector sy (0, 1);
			while (x + v.x() < 2) v += sx;
			while (x + v.x() > m_Width - 3) v -= sx;
			while (y + v.y() < 2) v += sy;
			while (y + v.y() > m_Height - 3) v -= sy;
		}

		void randomize (Vector&, int);
		double xMax() const {
			return (double) m_Width;
		}
		double yMax() const {
			return (double) m_Height;
		}

		uint16_t* fillField (int);

};

#endif // VECTORFIELD_H
