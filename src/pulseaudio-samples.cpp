
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "driver.h"

#ifdef HAVE_PULSE
#include <pulse/simple.h>
#include <pulse/error.h>
#endif // HAVE_PULSE

#include <iostream>
#include <cstdlib>
#include "pulseaudio-samples.h"
#include "ringbuffer.h"

using namespace std;

#ifdef HAVE_PULSE

int sample_pulseaudio (void* arg) {
	sample_param_t* p = (sample_param_t*) arg;
	RingBuffer* ringbuffer = p->ringbuffer;
	bool* run = p->run;
	int err;

	pa_sample_spec ss;
	ss.format = PA_SAMPLE_S16LE;
	ss.channels = 2;
	ss.rate = 44100;
	pa_buffer_attr ba;
	ba.maxlength = -1;
	ba.fragsize = 128;
	char *buf = (char*) malloc (128);

	pa_simple *s = pa_simple_new (NULL, "fische", PA_STREAM_RECORD, NULL, "pcm_in", &ss, NULL, &ba, &err);
	if (!s) {
		cerr << "ERROR: cannot open PulseAudio stream: " << pa_strerror (err) << endl;
		exit (EXIT_FAILURE);
	}

	cout << "* created thread for sound sample acquisition (pulse)" << endl;

	int n = 0;
	while (*run) {
		n = pa_simple_read (s, buf, 128, &err);
		if (n < 0) {
			cerr << "ERROR: PulseAudio read failed: " << pa_strerror (err) << endl;
			exit (EXIT_FAILURE);
		}

		else {
			// write data
			ringbuffer->insert (buf, 128);
		}
	}
	cout << "* stopping sound sampling thread" << endl;
	pa_simple_free (s);
	free (buf);
	buf = NULL;
	return NULL;
}

#else // HAVE_PULSE
int sample_pulseaudio (void* arg) {
	cout << "ERROR: PulseAudio driver not enabled at compile time." << endl;
	cout << "       Please use a different audio input driver!" << endl;
	exit (EXIT_FAILURE);
}
#endif // HAVE_PULSE
