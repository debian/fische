
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

class RingBuffer
{
	private:
		static RingBuffer* ms_instance;

	private:
		RingBuffer (int);
		~RingBuffer();

	public:
		static RingBuffer* instance (int size = 1200);
		void release();

	public:
		void insert (char*, int);
		char* get();
		int size() const;
		bool ready() const;

	private:
		char*		mp_Buffer;
		char*		mp_ExportBuffer;
		int			m_Cursor;
		bool		m_IsLocked;
		int			m_Size;
		int			m_Inserted;
		bool		m_IsFull;
};
#endif // RINGBUFFER_H
