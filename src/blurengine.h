/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLURENGINE_H
#define BLURENGINE_H

class SdlScreen;
class VectorField;

class BlurEngine
{
	private:
		static BlurEngine* ms_instance;

	private:
		BlurEngine();
		~BlurEngine();

	public:
		static BlurEngine* instance();
		void release();

	public:
		void blur();

	private:
		VectorField*	mp_Vectorfield;
		SdlScreen*		mp_SdlScreen;
		int				m_X0;
		int				m_Y0;
		int				m_Width;
		int				m_Height;
		int				m_Cpus;
		void*			mp_Buffer;

		void emptyBuffer();
		void writeBuffer();
		void blurTrad();
		void blurTrad_MT();
};

#endif // BLURENGINE_H
