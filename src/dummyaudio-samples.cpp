
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "driver.h"

#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <cmath>
#include <unistd.h>
#include <SDL/SDL.h>
#include "dummyaudio-samples.h"
#include "ringbuffer.h"

using namespace std;

const double pi = acos (-1);

int sample_dummyaudio (void* arg)
{
	sample_param_t* p = (sample_param_t*) arg;
	RingBuffer* ringbuffer = p->ringbuffer;
	bool* run = p->run;

	double fps = 173460 / ringbuffer->size();
	int ticks_per_frame = 1000 / fps;
	unsigned int last_ticks = 0;
	void* buf = malloc (ringbuffer->size());

	while (*run) {
		while (SDL_GetTicks() < last_ticks + ticks_per_frame) {
			usleep (1000);
			if (SDL_GetTicks() < last_ticks) break;
		}
		last_ticks = SDL_GetTicks();
		if (last_ticks > 1e9) last_ticks = 0; // fix for late SDL initialisation
		double phi = 0;
		int I = rand() % 0x8000;
		int n = rand() % 200;
		for (int i = 0; i < ringbuffer->size() / 2; i += 2) {
			* ( ( (int16_t*) buf) + i) = I * sin (phi);
			* ( ( (int16_t*) buf) + i + 1) = I * cos (phi);
			phi += pi / ringbuffer->size() * n;
		}
		ringbuffer->insert ( (char*) buf, ringbuffer->size());
	}
	cout << "* stopping sound sampling thread" << endl;
	return 0;
}
