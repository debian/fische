
/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "driver.h"

#ifdef HAVE_PORTAUDIO
#include <portaudio.h>
#endif // HAVE_PORTAUDIO

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include "portaudio-samples.h"
#include "ringbuffer.h"

using namespace std;

#ifdef HAVE_PORTAUDIO

static int paCallback (const void* in, void* out, unsigned long frames, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* rb)
{
	RingBuffer* ringbuffer = static_cast<RingBuffer*> (rb);
	ringbuffer->insert ( (char*) in, frames * 4);
	return 0;
}

int sample_portaudio (void* arg)
{
	sample_param_t* p = (sample_param_t*) arg;
	RingBuffer* ringbuffer = p->ringbuffer;
	bool* run = p->run;
	const char* device = p->device;

	int err = Pa_Initialize();
	if (err != paNoError) {
		cout << "PortAudio error: " << Pa_GetErrorText (err) << endl;
		exit (EXIT_FAILURE);
	}

	bool help = false;
	// help ?
	if (!strncmp (device, "help", 4)) help = true;

	// device specified as index
	int index = atoi (device);

	// device "default"
	if (!strncmp (device, "default", 7)) index = Pa_GetDefaultInputDevice();

	// other device
	else {
		const PaDeviceInfo* info;
		int numDevices = Pa_GetDeviceCount();
		for (int i = 0; i < numDevices; i++) {
			info = Pa_GetDeviceInfo (i);
			if (help) cout << "+ device #" << i << ": " << info->name << endl;
			if (strncmp (info->name, device, strlen (device)) == 0) {
				index = i;
			}
		}
	}
	if (help) exit (EXIT_SUCCESS);

	cout << "* using portaudio device index " << index << endl;

	PaStreamParameters params;
	params.device = index;
	params.channelCount = 2;
	params.sampleFormat = paInt16;
	params.suggestedLatency = 0.0007;
	params.hostApiSpecificStreamInfo = NULL;

	PaStream* stream;

	err = Pa_OpenStream (&stream, &params, NULL, 44100, 32, paNoFlag, paCallback, (void*) ringbuffer);
	if (err != paNoError) {
		cout << "PortAudio error: " << Pa_GetErrorText (err) << endl;
		exit (EXIT_FAILURE);
	}

	err = Pa_StartStream (stream);
	if (err != paNoError) {
		cout << "PortAudio error: " << Pa_GetErrorText (err) << endl;
		exit (EXIT_FAILURE);
	}

	cout << "* created thread for sound sample acquisition (portaudio)" << endl;
	while (*run) {
		usleep (100000);
	}

	cout << "* stopping sound sampling thread" << endl;

	err = Pa_StopStream (stream);
	if (err != paNoError) {
		cout << "PortAudio error: " << Pa_GetErrorText (err) << endl;
		exit (EXIT_FAILURE);
	}

	return 0;
}

#else // HAVE_PORTAUDIO
int sample_portaudio (void* arg)
{
	cout << "ERROR: PortAudio driver not enabled at compile time." << endl;
	cout << "       Please use a different audio input driver!" << endl;
	exit (EXIT_FAILURE);
}
#endif // HAVE_PORTAUDIO
