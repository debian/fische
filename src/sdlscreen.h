/*
 * fische-3.2
 * Copyright (C) Marcel Ebmer 2009-2010 <marcel@26elf.at>
 *
 * fische-3.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * fische-3.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDLSCREEN_H
#define SDLSCREEN_H

#define CONTINUE 0
#define EXIT 1
#define PAUSE 2
#define TOGGLE_FULLSCREEN 4
#define MOUSE 8
#define PLUS 16
#define MINUS 32
#define TOGGLE_NERVOUS 64

class SDL_Surface;

class SdlScreen
{
	private:
		static SdlScreen* ms_instance;

	private:
		SdlScreen (int, int, bool);
		~SdlScreen();

	public:
		static SdlScreen* instance (int x = 300, int y = 200, bool fs = false);
		void release();
	
	public:
		int height() const;
		int width() const;
		void* pixels();
		void lock();
		void unlock();
		int pitch() const;
		void update();
		int checkEvents();
		bool toggleFullscreen();

	private:
		SDL_Surface*	mp_Screen;
		int				m_Width;
		int				m_Height;
		bool			m_IsFullscreen;
};


#endif // SDLSCREEN_H
